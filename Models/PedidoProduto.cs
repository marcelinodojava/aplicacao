﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace aplicacao.Models{

    public class PedidoProduto{

        public int PedidoId {get; set;}
	public Pedido Pedido {get; set;}

	public int ProdutoId {get; set;}
	public Produto Produto {get; set;}

    }

}
