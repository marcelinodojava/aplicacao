﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace aplicacao.Models{

    public class Fornecedor{

        public int FCnpjId {get; set;}
        public string FNome {get; set;}
        public string FEmail {get; set;}
        public int FTelefone {get; set;}
        public string FEstado {get; set;}
        public string FCidade {get; set;}
        public int FCEP {get; set;}
        public string FBairro {get; set;}
        public string FRua {get; set;}
        public int FNumero {get; set;}
        public string FComplemento {get; set;}

	public ICollection<FornecedorProduto> FornecedorProdutos {get; set;}

    }

}
