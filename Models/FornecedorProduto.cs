﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace aplicacao.Models{

    public class FornecedorProduto{

        public int FCnpjId {get; set;}
	public Fornecedor Fornecedor {get; set;}
	
	public int ProdutoId {get; set;}
	public Produto Produto {get; set;}

    }

}
