﻿using aplicacao.Models;
using Microsoft.EntityFrameworkCore;

namespace aplicacao.Models
{
  public class ApiDbContext : DbContext
  {
    public ApiDbContext(DbContextOptions<ApiDbContext> options)
        : base(options)
    { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
	// ClientePedido
      modelBuilder.Entity<ClientePedido>()
          .HasKey(tr => new { tr.CnpjId, tr.PedidoId });
      modelBuilder.Entity<ClientePedido>()
          .HasOne(tr => tr.Cliente)
          .WithMany(t => t.ClientePedidos)
          .HasForeignKey(tr => tr.CnpjId);
      modelBuilder.Entity<ClientePedido>()
          .HasOne(tr => tr.Pedido)
          .WithMany(r => r.ClientePedidos)
          .HasForeignKey(tr => tr.PedidoId);

	// FornecedorProduto
    modelBuilder.Entity<FornecedorProduto>()
          .HasKey(tr => new { tr.FCnpjId, tr.ProdutoId });
      modelBuilder.Entity<FornecedorProduto>()
          .HasOne(tr => tr.Fornecedor)
          .WithMany(t => t.FornecedorProdutos)
          .HasForeignKey(tr => tr.FCnpjId);
      modelBuilder.Entity<FornecedorProduto>()
          .HasOne(tr => tr.Produto)
          .WithMany(r => r.FornecedorProdutos)
          .HasForeignKey(tr => tr.ProdutoId);

	// PedidoProduto
    modelBuilder.Entity<PedidoProduto>()
          .HasKey(tr => new { tr.PedidoId, tr.ProdutoId });
      modelBuilder.Entity<PedidoProduto>()
          .HasOne(tr => tr.Pedido)
          .WithMany(t => t.PedidoProdutos)
          .HasForeignKey(tr => tr.PedidoId);
      modelBuilder.Entity<PedidoProduto>()
          .HasOne(tr => tr.Produto)
          .WithMany(r => r.PedidoProdutos)
          .HasForeignKey(tr => tr.ProdutoId);

    }

    public DbSet<Pedido> Pedidos { get; set; }
    public DbSet<Cliente> Clientes { get; set; }
    public DbSet<Fornecedor> Fornecedores { get; set; }
  }
}
