﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace aplicacao.Models{

    public class Produto{


        public int ProdutoId {get; set;}

        public float ValorProduto {get; set;}

    public ICollection<FornecedorProduto> FornecedorProdutos {get; set;}
    public ICollection <PedidoProduto> PedidoProdutos {get; set;}
    }

    

}
